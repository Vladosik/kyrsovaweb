// *-----------------------------------------------------Бургер
let burger = document.querySelector('.burger');
let headMenu = document.querySelector('.head-menu');
const body = document.querySelector('body');
burger.addEventListener('click', e => {
    burger.classList.toggle('active');
    if (burger.classList.contains('active')) {
        headMenu.classList.add('active');
        body.classList.add('active');
    } else {
        headMenu.classList.remove('active');
        body.classList.remove('active');
    }
})
// -----------------------------------------------------------Всплив вікно
const popupLinks = document.querySelectorAll('.popup-link');
const lockPadding = document.querySelectorAll('.lock-padding')
let unlock = true;
const timeout = 800;

if (popupLinks.length > 0) {
    for (let index = 0; index < popupLinks.length; index++) {
        const popupLink = popupLinks[index];
        popupLink.addEventListener('click', function (e) {
            const popupName = popupLink.getAttribute('href').replace('#', '');
            const currentPopup = document.getElementById(popupName);
            popupOpen(currentPopup);
            e.preventDefault(); // забороняє перезагружати сторінку
            console.log('1')
        });
    }
}
const popupCloseIcon = document.querySelector('.close-popup');
if(popupCloseIcon.length > 0){
    for (let index = 0; index < popupCloseIcon.length; index++) {
        const el = popupCloseIcon[index]
    el.addEventListener('click', function (e) {
            popupClose(popupCloseIcon.closest('.popup')); // йдем в родітельский елемент
            e.preventDefault();
            console.log('2')
        });
    }
}

function popupOpen(currentPopup) {
    if (currentPopup && unlock) {
        const popupActive = document.querySelector('.popup.open');
        if (popupActive) {
            popupClose(popupActive, false);
        } else {
            bodyLock();// блочить скрол
        }
        currentPopup.classList.add('open');
        currentPopup.addEventListener('click', e => {
            if (!e.target.closest('.popup-content')) {
                popupClose(e.target.closest('.popup')) // єслі нажати на затемнення то закриє
            }
        })
        console.log('3')
    }
}

function popupClose(popupActive, doUnlock = true) {
    if (unlock) {
        popupActive.classList.remove('open');
        if (doUnlock) {
            bodyUnlock();
        }
    }
    console.log('4')
}

function bodyLock() {
    const lockPaddingValue = window.innerWidth - document.querySelector('.wrapper').offsetWidth + 'px'// вищитує ширину скрола
  if (lockPadding.length > 0) {
      for (let index = 0; index < lockPadding.length; index++) {
          const el = lockPadding[index]
          el.style.paddingRight = lockPaddingValue;
      }
  }
    body.style.paddingRight = lockPaddingValue;
    body.classList.add('lock');
    unlock = false;
    setTimeout(function () {
        unlock = true;
    }, timeout);
    console.log('5')
}

function bodyUnlock() {
    setTimeout(function () {
        lockPadding.style.paddingRight = '0px';
        body.style.paddingRight = '0px';
        body.classList.remove('lock');
    }, timeout)
    console.log('6')
}