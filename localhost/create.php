<?php

require './libs/rb-mysql.php';

if (!$_SESSION['user']) header('Location: /');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
            integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="style/create.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body>
<div class="wrapper">
    <?php require './views/layouts/header.php';
    $categories = R::findAll('categories');
    ?>
    
    <div class="create">
        <div class="create-container">
            <div class="create-body">
                <form id="form">
                    <input type="text" id="title" placeholder="Заговолок"><br>
                    <input type="text" id="image" placeholder="Ссылку на картинку"><br>
                    <textarea type="text" id="text" placeholder="Описаные"></textarea><br>
                    <p><select id="topic">
                            <option disabled selected>Выберите категорию</option>
                           <?php 
                           foreach ($categories as $category):
                           ?>
                               <option value="<?= $category['id']?>"><?= $category['title']?></option>
                            <?php
                           endforeach;
                            ?>
                        </select></p>
                    <button type="submit" class="btnFrom">Добавить пост</button>
                </form>
            </div>
        </div>
    </div>
    <?php require './views/layouts/footer.php'; ?>
    <div class="popup" id="popup">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Регистрация</h2>
                </div>
                <div class="popup-text">
                    <form action="site.html" name="f1">
                        <input type="text" placeholder="Имя" name="firstName">
                        <input type="text" placeholder="Фамилия" name="secondName">
                        <input type="email" placeholder="Email" name="email">
                        <input type="password" placeholder="Пароль" name="password">
                        <input type="password" placeholder="Подтвердите пароль" name="passwordAgain">
                        <input type="submit" placeholder="Регистрация" name="btnSubmit" class="submit">
                    </form>
                    <p>Уже есть аккаунт?<a href="#popup-2" class="popup-link"> Войти</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" id="popup-2">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Вход</h2>
                </div>
                <div class="popup-text">
                    <form action="site.html" name="f1">

                        <input type="email" placeholder="Email" name="email">
                        <input type="password" placeholder="Пароль" name="password">

                        <input type="submit" placeholder="Войти" name="btnSubmit" class="submit">
                    </form>
                    <p>Нет акаунта?<a href="#popup" class="popup-link"> Регистрация</a></p>
                </div>
            </div>
        </div>
    </div>
    <script>
        const form = $('#form');



        form.on('submit', e => {
            e.preventDefault();
            const title = $('#title').val()
            const text = $('#text').val()
            const image = $('#image').val()
            const category_id = $('#topic').val();
            $.ajax({
                url: '/actions/post/create.php',
                method: 'post',
                data: {
                    title,
                    text,
                    image,
                    category_id
                },
                success: res => {
                    location.replace('/post.php?id=' + res)
                }
            })
        })
    </script>
    <script src="js/main.js"></script>
</body>

</html>