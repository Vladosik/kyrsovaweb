<?php require './libs/rb-mysql.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
            integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
            crossorigin="anonymous"></script>

</head>

<body>
<div class="wrapper">
    <?php require './views/layouts/header.php'; ?>
    <div class="content">
        <div class="container">
            <div class="content-body">
                <ul class="content-list">
                    <?php
                    $page = $_GET['page'];
                    $postsPerPage = 5;
                    $page = $page ? $page : 1;
                    $offset = ($page - 1) * $postsPerPage;

                    $total_pages = ceil(R::count('posts') / $page);

                    $search = $_GET['search'];


                    // --------------------------------------------поиск
                    if ($search) {
                        $posts = R::findAll('posts', 'WHERE title LIKE ? or content LIKE ? LIMIT ?, ?', [
                            "%$search%",
                            "%$search%",
                            $offset,
                            $postsPerPage
                        ]);
                    } elseif ($_GET['category']) {

                        $posts = R::findAll('posts', 'WHERE category_id = ?', [
                            $_GET['category'],
                        ]);

                    } else {

                        $posts = R::getAll('SELECT * FROM `posts` LIMIT ?, ?', [
                            $offset,
                            $postsPerPage
                        ]);

                    }
                        if (empty($posts)){
                        echo 'Записів не знайдено';
                    }

                    foreach ($posts as $row) : ?>

                        <li class="article">
                            <div class="article-after-date">
                                <h4 class="article-date"><?= $row['date'] ?></h4>
                                <div class="article-btn">
                                    <?php if($_SESSION['user']->id == $row['user_id']): ?>
                                    <a href="/edit.php?id=<?= $row['id'] ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                    </svg>

                                    </a>
                                    <a href="/actions/post/delete.php?id=<?= $row['id'] ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                    </svg>
                                    </a>
                                    <?php elseif ($_SESSION['user']->id === '1'): ?>
                                        <a href="/edit.php?id=<?= $row['id'] ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                            </svg>

                                        </a>
                                        <a href="/actions/post/delete.php?id=<?= $row['id'] ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                            </svg>
                                        </a>
                                    <?php else: ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <ul class="article-list">
                                <li>

                                    <p class="view" value="0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                            <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                                            <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                                        </svg>
                                        <?php
                                        if ($row['views']) {
                                            echo $row['views'];
                                        } else echo '0';
                                        ?>
                                    </p>

                                    <div class="article-content">
                                        <div class="article-img">
                                            <a href="/post.php?id=<?= $row['id'] ?>" class="viewCheck">
                                                <?=
                                                $image = "<img src='{$row['image']}'>";
                                                ?>
                                            </a>
                                        </div>
                                        <a href="/post.php?id=<?= $row['id'] ?>">
                                            <div class="article-content-text">
                                                <div class="article-text">
                                                    <h4 class="article-title"><?= $row['title']; ?></h4>

                                                    <?= mb_strimwidth($row['content'], 0, 400, '...'); ?>

                                                </div>

                                                <div class="article-person">
                                                    <hr>
                                                    <a href="">
                                                        <?php
                                                        $user = R::findOne('users', 'id = ?', [$row['user_id']]);
                                                        echo $user->first_name . ' ' . $user->last_name;
                                                        ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
    <div class="numberOfPage">
        <div class="container">
            <div class="numberOfPageBody">
                <ul class="numPage">
                    <?php if ($page != 1): ?>
                        <li class="num">   <a href="?page=<?= $page - 1; ?>">Назад</a>    </li>
                    <?php endif; ?>



                    <?php if ($total_pages != 1): ?>
                        <li class="num">    <a href="?page=<?= $page + 1; ?>">Вперед</a>    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php require './views/layouts/footer.php'; ?>

    <div class="popup" id="popup-2">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Регистрация</h2>
                </div>
                <div class="popup-text">
                    <form id="register-form">
                        <input type="text" placeholder="Имя" name="firstName" id="firstname">
                        <input type="text" placeholder="Фамилия" name="lastName" id="lastname">
                        <input type="email" placeholder="Email" name="email" id="email">
                        <input type="password" placeholder="Пароль" name="password" id="password">
                        <input type="password" placeholder="Подтвердите пароль" name="passwordAgain" id="passwordAgain">
                        <input type="submit" value="Регистрация" class="submit">
                    </form>
                    <p>Уже есть аккаунт?<a href="#popup" class="popup-link"> Войти</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" id="popup">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Вход</h2>
                </div>
                <div class="popup-text">
                    <form id="login-form">

                        <input type="email" placeholder="Email" id="email-login">
                        <input type="password" placeholder="Пароль" id="password-login">

                        <input type="submit" value="Войти" id="btnSubmit" class="submit">
                    </form>
                    <p>Нет акаунта?<a href="#popup-2" class="popup-link"> Регистрация</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/main.js"></script>
<script>
    const registerForm = $('#register-form');
    registerForm.on('submit', e => {
        e.preventDefault();
        const firstname = $('#firstname').val();
        const lastname = $('#lastname').val();
        const email = $('#email').val();
        const password = $('#password').val();
        const passwordAgain = $('#passwordAgain').val();

        $.ajax({
            url: '/actions/auth/register.php',
            method: 'post',
            data: {
                firstname,
                lastname,
                email,
                password,
                passwordAgain
            },
            success: function (res) { // res це ответ вiд серва
                if (res === 'emptyFirstname') {
                    alert('Ввседите имя')
                } else if (res === 'emptyLastname') {
                    alert('Введите фамилию')
                } else if (res === 'passwordNotConfirm') {
                    alert('Пароли не совпадают')
                } else if (res === 'emailExist') {
                    alert('Email занят')
                } else {
                    location.replace('/');
                }

            }

        })
    })

    const loginForm = $('#login-form');
    loginForm.on('submit', e => {
        e.preventDefault();

        const email = $('#email-login').val();
        const password = $('#password-login').val();


        $.ajax({
            url: '/actions/auth/login.php',
            method: 'post',
            data: {
                email,
                password
            },
            success: function (res) { // res це ответ вид серва
                if (res === 'userNotFound') {
                    alert('Пользователь не найден или неверный пароль')
                } else {
                    location.replace('/')
                }
            }

        })
    })
</script>
</body>

</html>