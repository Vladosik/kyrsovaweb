<?php
require '../../libs/rb-mysql.php';

$firstName = $_POST['firstname'];
$lastName = $_POST['lastname'];
$email = $_POST['email'];
$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
$passwordAgain = $_POST['passwordAgain'];

$errors = [];

if (trim($firstName) === '') {
    $errors[] = 'emptyFirstname';
}
if (trim($lastName) === '') {
    $errors[] = 'emptyLastname';
}
if (!password_verify($passwordAgain, $password)) {
    $errors[] = 'passwordNotConfirm';
}
if(R::findOne('users', 'email LIKE ?', [$email])){
    $errors[] = 'emailExist';
}


$user = R::dispense('users');

$user->firstName = $firstName;
$user->lastName = $lastName;
$user->email = $email;
$user->password = $password;

if (empty($errors)) {
    R::store($user);
    echo(json_encode($user));

} else {
    echo(array_shift($errors));
}
?>