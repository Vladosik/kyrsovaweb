<?php
require '../../libs/rb-mysql.php';

$email = $_POST['email'];
$password = $_POST['password'];

$user = R::findOne('users', 'email = ?', [$email]);

if ($user) {
    if (password_verify($password, $user->password)) {
        $_SESSION['user'] = $user;
    } else {
        echo 'userNotFound';
    }
} else {
    echo 'userNotFound';
}