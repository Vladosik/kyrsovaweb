<?php
require '../../libs/rb-mysql.php';
if (!$_SESSION['user']) die;

$id = $_POST['id'];
$data = $_POST;
$edit = R::load('posts', $id);

$edit->title = $data['title'];
$edit->content = $data['text'];
$edit->image = $data['image'];
$edit->category_id = $data['category_id'];


R::store($edit);

