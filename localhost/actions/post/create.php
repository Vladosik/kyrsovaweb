<?php

require '../../libs/rb-mysql.php';

if (!$_SESSION['user']){
    header('Location: /');
}

$title = $_POST['title'];
$text = $_POST['text'];
$userId = $_SESSION['user']->id;
$image = $_POST['image'];
$category_id = $_POST['category_id'];

$post = R::dispense('posts');
$post->title = $title;
$post->content = $text;
$post->user_id = $userId;
$post->image =$image;
$post->date = date('Y-m-d');
$post->category_id = $category_id;

$postId = R::store($post);

echo $postId;
