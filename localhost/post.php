<?php require './libs/rb-mysql.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="style/full-article.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width">
</head>
<body>
<div class="wrapper">
    <?php require './views/layouts/header.php'; ?>
    <div class="full-content">
        <div class="container-article">
            <?php

            $post = R:: findOne('posts', 'id = ?', [$_GET['id']]);

            $viewsCount = $post['views'];
            if (!$viewsCount) {
                $viewsCount = 0;
            }
            $viewsCount = $viewsCount + 1;
            $post->views = $viewsCount;
            R::store($post);


            echo '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                                <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                                <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                            </svg> ' . $viewsCount;
            echo '<h2 class="title">' . $post['title'] . '</h2>';
            echo '<p class="text">' . $post['content'] . '</p>';
            $user = R::findOne('users', 'id = ?', [$post['user_id']]);

            echo '  <div class="article-person">
                       <hr>
                        <a href="">' . $user->first_name . ' ' . $user->last_name . ' </a>
                                                                                             </div>';
            ?>
        </div>
    </div>
    <?php require './views/layouts/footer.php'; ?>
    <div class="popup" id="popup">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Регистрация</h2>
                </div>
                <div class="popup-text">
                    <form action="site.html" name="f1">
                        <input type="text" placeholder="Имя" name="firstName">
                        <input type="text" placeholder="Фамилия" name="secondName">
                        <input type="email" placeholder="Email" name="email">
                        <input type="password" placeholder="Пароль" name="password">
                        <input type="password" placeholder="Подтвердите пароль" name="passwordAgain">
                        <input type="submit" placeholder="Регистрация" name="btnSubmit" class="submit">
                    </form>
                    <p>Уже есть аккаунт?<a href="#popup-2" class="popup-link"> Войти</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="popup" id="popup-2">
        <div class="popup-body">
            <div class="popup-content">
                <a href="" class="popup-close close-popup">x</a>
                <div class="popup-title">
                    <h2>Вход</h2>
                </div>
                <div class="popup-text">
                    <form action="site.html" name="f1">

                        <input type="email" placeholder="Email" name="email">
                        <input type="password" placeholder="Пароль" name="password">

                        <input type="submit" placeholder="Войти" name="btnSubmit" class="submit">
                    </form>
                    <p>Нет акаунта?<a href="#popup" class="popup-link"> Регистрация</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/main.js"></script>
</body>
</html>
