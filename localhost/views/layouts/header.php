<header class="header lock-padding">
    <div class="container">
        <div class="header-body">
            <a href="index.php" class="logo">
                <span class="first-text">Music</span>
                <span class="second-text">News</span>
            </a>
            <div class="burger"><span>

                </span></div>
            <?php
            $categories = R::findAll('categories');
            ?>
            <nav class="head-menu">
                <ul class="head-list">
                    <?php
                    foreach ($categories as $category):
                    ?>
                    <li><a href="/?category=<?= $category->id ?>" class="list"><?= $category->title ?></a></li>
                    <?php
                    endforeach;
                    ?>
                    <?php if ($_SESSION['user']): ?>
                        <li><a href="/" class="list">
                                <?= $_SESSION['user']->first_name; ?>
                            </a></li>
                    <li><a href="/create.php" class="list">Добавить статью</a></li>
                        <li><a href="/actions/auth/logout.php" class="list">Выход</a></li>
                    <?php else: ?>
                        <li><a href="#popup" class="list popup-link"><i class="fa fa-user icon"></i></a></li>
                    <?php endif; ?>
                    <li>
                        <form class="head-search" action="/" method="get">
                            <input type="text" placeholder="Искать здесь..." name="search" value="<?= $_GET['search'] ?>">
                            <button type="submit"></button>
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>